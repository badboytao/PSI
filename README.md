# 关于PSI

---

>PSI是一款基于SaaS模式(Software as a Service软件即服务)的企业管理软件。PSI以商贸企业的核心业务：采购、销售、库存（进销存）为切入点，最终目标是行业化的ERP解决方案。
>
>为什么说PSI是一款基于SaaS的企业管理软件？详见：https://my.oschina.net/u/134395/blog/1551258

# PSI演示

---

>PSI的演示见：<a target="_blank" href="http://psi.butterfly.mopaasapp.com">http://psi.butterfly.mopaasapp.com</a>
>
>

# PSI运行环境

---

>
>`PHP 7+`, `MySQL 5.5+`
>
>`安装文档`见：http://psi.butterfly.mopaasapp.com/help/01.html
>
>如果在寻找Java编写的ERP，请访问：https://gitee.com/crm8000/ABS


# PSI的开源协议

---

>PSI的开源协议为GPL v3
>
>允许任何人完全私有化PSI，详见：https://my.oschina.net/u/134395/blog/1543536

# 如何参与PSI的研发

---

> 《如何为开源项目做出自己的贡献》- https://my.oschina.net/u/134395/blog/670779
>
> 《为什么PSI不接受外部代码》- https://my.oschina.net/u/134395/blog/1580780

# PSI相关项目

---

> PSI使用帮助：https://gitee.com/crm8000/PSI_Help
>
> 帮助手册采用Markdown格式编写，使用gitbook工具构建后集成到PSI代码里面

# 如需要技术支持，请联系

---

>1、 普通QQ群： <a target="_blank" href="http://shang.qq.com/wpa/qunwpa?idkey=64808ce24f2a3186ccb1f37aad9ed591bcc4fb257d09749753aca98c6c73e400">414474186</a>
>
>2、 PSI 免费VIP QQ群：108111233
> 加入本群的要求：1、实名；2、认真地在应用PSI，并愿意分享实际应用案例。
>
>3、 付费技术支持QQ群：498771245
>
>PSI的技术难度在哪里？参见：https://my.oschina.net/u/134395/blog/1554724
>
>PSI二次开发中必须掌握ExtJS技术，`ExtJS知识点`参见：https://my.oschina.net/u/134395/blog?catalog=3420148&temp=1513571513683
>

# 致谢

---

>PSI使用了如下开源软件，没有你们，就没有PSI
> 
>1、PHP (http://php.net/)
>
>2、MySQL (http://www.mysql.com/)
>
>3、ExtJS 4.2 (http://www.sencha.com/)
>
>4、ThinkPHP 3.2.3 (http://www.thinkphp.cn/)
>
>5、乱码 / pinyin_php (https://git.oschina.net/cik/pinyin_php)
>
>6、PHPExcel (https://github.com/PHPOffice/PHPExcel)
>
>7、TCPDF (http://www.tcpdf.org/)
>
>8、MUI (http://dev.dcloud.net.cn/mui/)